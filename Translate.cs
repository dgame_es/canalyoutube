using UnityEngine;
using System;
using System.IO;

namespace Translate
{
	[Serializable]
	public class Elements
	{
		public string test = "Undefined";
	}

	public class Manager : MonoBehaviour {

		// Atributos publicos
		public string Language;
		public Elements translate;

		// Use this for initialization
		void Start () 
		{
			string data = File.ReadAllText (Path ());
			translate = JsonUtility.FromJson<Elements> (data);
		}

		//Metodos privados

		/* Obtener ruta del idioma a traducir */
		string Path ()
		{
			string path = Application.dataPath + "/Resources/Languages/";

			Language = (PlayerPrefs.HasKey ("Language")) ? PlayerPrefs.GetString ("Language") : LanguageClient ( path );

			return path + Language + ".json";
		}

		/* Detectar el idioma del cliente */
		string LanguageClient ( string path )
		{
			string lang = Application.systemLanguage.ToString ();

			lang = (File.Exists( path + lang + ".json" )) ? lang : "English";

			PlayerPrefs.SetString ("Language", lang);

			return lang;
		}


	}
}
